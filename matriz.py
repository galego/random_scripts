def criarMatriz(m, n, matriz):
    for i in range(1, m+1):
        linha = []
        for j in range(1, n+1):
            num = int(input(f"Número da posição [{i},{j}]: "))
            linha.append(num)
        matriz.append(linha)


def mudarElemento(linha, coluna, valor, matriz):
    matriz[linha-1][coluna-1] = valor

matriz = []
l = int(input("Linhas da matriz: "))
c = int(input("Colunas da matriz: "))

criarMatriz(l, c, matriz)
print(matriz)
linha = int(input("Linha a mudar: "))
coluna = int(input("Coluna a mudar: "))
valor = int(input("Novo valor da posição [{},{}]: ".format(linha, coluna)))

mudarElemento(linha, coluna, valor, matriz)
print(matriz)
