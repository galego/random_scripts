"""
"Converter" arquivos .ipynb para .py
Convert .ipynb files into .py files

Uso / Usage:
    python ipynb_converter.py <ipynb_file.ipynb> <new_file.py>
"""
import sys
import json

filename, new_file = sys.argv[1:]

with open(filename, "r") as f:
    content = json.load(f)


cells = content["cells"]

if not new_file.endswith(".py"):
    save_file = open(new_file + ".py", "w")
else:
    save_file = open(new_file, "w")

for cell in cells:
    save_file.writelines(cell["source"])
    save_file.write("\n\n")

save_file.close()
