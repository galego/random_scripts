class Conversor():
  
    def converter_para_bin(self, texto):
        convertido = ""
        for c in texto:
            tmp = bin(ord(c))
            letra = tmp.replace("b", "")
            convertido += letra + " "
        return convertido.strip()


    def converter_para_hex(self, texto):
        convertido = ""
        for c in texto:
            tmp = hex(ord(c))
            letra = tmp.replace("0x", " ")
            convertido += letra
        return convertido


    def bin_to_string(self, binario):
        traduzido = []
        lista_bin = binario.split()
        for b in lista_bin:
            cont, indice = 0, 0
            b = b[::-1]
            for c in b:
                if c == "1":
                    cont = cont + (2**(b.find(c, indice)))
                indice += 1
            traduzido.append(chr(cont))
        return "".join(traduzido)


    def hex_to_string(self, hexa):
        dicio = {"a":10,"b":11,"c":12,"d":13,"e":14,"f":15}
        traduzido = []
        lista_hexa = hexa.split()
        for letra in lista_hexa:
            in1, in2 = letra[0], letra[1]
            tmp = int(dicio.get(in1, in1))*16 + int(dicio.get(in2, in2))
            traduzido.append(chr(tmp))
        return "".join(traduzido)


if __name__ == "__main__":
    conversor = Conversor()
    frase = "Grande é o Senhor e mui Digno de louvores"

    hexa = conversor.converter_para_hex(frase)
    binario = conversor.converter_para_bin(frase)
    decodificado_bin = conversor.bin_to_string(binario)
    print(hexa)
    print(binario)
    print(decodificado_bin)

    print(conversor.hex_to_string(hexa))
