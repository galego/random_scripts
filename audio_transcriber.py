"""
https://pythonbasics.org/Transcribe-Audio/
------------------------------------------
pip install pydub, speechrecognition
"""
import speech_recognition as sr
from os import path
from pydub import AudioSegment

# Convert mp3 file to wav
sound = AudioSegment.from_mp3("transcript.mp3")
sound.export("transcript.wav", format="wav")

# Transcribe audio file
AUDIO_FILE = "transcript.wav"

# Use the audio file as the audio source
r = sr.Recognizer()
with sr.AudioFile(AUDIO_FILE) as source:
    audio = r.record(source)  # Read the entire audio file
