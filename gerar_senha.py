import random

def senha():
    simbolos = [
        "'", "!", "@", "#", "$", "%", "&", '"', "*", "(", ")", "-", "_",
        "+", "=", "§", "/", "\\", "|", "<", ">", ",", ".", ":", ";", "[",
        "]", "{", "}", "?",
    ]

    caracs = [chr(i) for i in range(65, 91)]
    caracs += [chr(i) for i in range(97, 123)]
    # caracs += [str(i) for i in range(10)]
    caracs += simbolos

    senha = ""

    print("\nVamos gerar uma senha 'quase, mas não totalmente, inteiramente' forte para você\n")
    tamanho = int(input("Informe o tamanho da senha que você deseja: "))

    while len(senha) < tamanho:
        c = random.choice(caracs)
        senha += c

    return senha

for i in range(5):
    print(f"Senha gerada: {senha()}")
