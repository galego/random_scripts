texto = 'O aprendizado automático (português brasileiro) ou a aprendizagem automática (português europeu) ou também aprendizado de máquina (português brasileiro) ou aprendizagem de máquina (português europeu) (em inglês: machine learning) é um subcampo da ciência da computação que evoluiu do estudo de reconhecimento de padrões e da teoria do aprendizado computacional em inteligência artificial. Em 1959, Arthur Samuel definiu aprendizado de máquina como o "campo de estudo que dá aos computadores a habilidade de aprender sem serem explicitamente programados"(livre tradução).'

nova_string = ""
inicio = fim = 0

while inicio != -1 and fim != -1:
    fim = texto.find("(", inicio)
    nova_string += texto[inicio:fim]
    inicio = texto.find(")", fim)
    inicio += 1

print(nova_string)
